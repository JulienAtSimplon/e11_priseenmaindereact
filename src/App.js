import React, { useState, useEffect } from 'react';
import Countries from './components/countries';

import localDataCountries from "./data/countries.json"
// import logo from './logo.svg';
import './App.css';

const api = "https://restcountries.com/v3.1/all"
// const localDataFile = "./src/data/countries.json"
// fetch(localDataFile).then((response) => response.json()).then(data => console.log(data));
// const localData = fetch(localDataFile).then(response => {return response.json();}).then(data => console.log(data));
// console.log(localData);


const FetchAll = () => {
	let [countries, setCountries] = useState(localDataCountries);
	let [isLoading, setIsLoading] = useState(false);
	let [isError, setIsError] = useState(false);

	useEffect(() => {

		const fetchData = async () => {
			await fetch(api)
				.then((response) => response.json()).then((data) => {
					setIsLoading(false);
					let sortedData = data.sort((a, b) => a.name.common.toLowerCase().localeCompare(b.name.common.toLowerCase()));
					sortedData.forEach(listItem => listItem.isSelected = false);
					setCountries(sortedData);
					// console.log("countries", countries);
				}).catch((error) => {
					setIsLoading(false);
					setIsError(true);
					console.log(error);
				});
		};

		const loadLocalData = () => {
			let sortedData = localDataCountries.sort((a, b) => a.name.common.toLowerCase().localeCompare(b.name.common.toLowerCase()));
			sortedData.forEach(listItem => listItem.isSelected = false);
			setCountries(sortedData);
		}

		// const extendData = () => {
		// 	let list = [];
		// 	countries.forEach(item => {
		// 		console.log(item);
		// 		list.add(item)});
		// 	list.forEach(listItem => listItem.isSelected = false);
		// 	setCountries(list)
		// 	console.log("test", countries);
		// };

		// fetchData();
		loadLocalData();
		// extendData();
	}, []);

	if (isLoading) {
		return <div>Loading...</div>;
	};

	return (
		<div>
			{countries && <Countries data={countries} />}
			{isError && <div>Error fetching data.</div>}
		</div>
	);
};

export default FetchAll;