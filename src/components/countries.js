import React, { useState, useEffect } from 'react';
import "../index.scss"
import Country from './country';
import SendBox from './sendbox';
// import { devCounter } from '../devTool';

export default function Countries( {data} ) {

	const [regionsList, setRegionsList] = useState([]);
	const [countOfSelectedCountries, setCountOfSelectedCountries] = useState(0);

	function childToParent(childData) {
		// console.log("childToParent :", childData.name.common, childData.isSelected);
		const index = data.findIndex((c) => c.cca3 === childData.cca3)
		data[index].isSelected = childData.isSelected
		selectionOfCountries()
		// console.log("data :", data[index].name.common, data[index].isSelected);
	};

	function selectionOfCountries() {
		let _counter = 0;
		data.forEach(c => {
			if (c.isSelected) { _counter += 1 }
		});
		setCountOfSelectedCountries(_counter)
	};
	
	function onClickRegion(region) {
		filterRegions(region)
	};
	
	function filterRegions(region) {
		const list = JSON.parse(JSON.stringify(regionsList))
		const index = list.findIndex((r) => r.name === region.name)
		list[index].isSelected = !list[index].isSelected;
		// console.log(region.name, region.isSelected);
		setRegionsList(list)
	};

	useEffect(() => {
		const regions = (data) => {
			let list = [];
			let list2 = [];
	
			data.forEach(item => {
				if (list.indexOf(item.region) === -1) { list.push(item.region) };
			});		
	
			list.forEach(item => list2.push( { name: item, isSelected: true } ));
			setRegionsList(list2)
			// console.log("regionsList", regionsList);
		};

		regions(data);
		// console.log("UseEf countries", data)
	}, [data]);

	useEffect(() => {
		// console.table(regionsList)
	}, [regionsList]);

	useEffect(() => {
		selectionOfCountries();
	});

	return (
		<div>
			<span><h1>All countries on earth</h1></span>
			{/* <p> rendu #{devCounter()} </p> */}
			<span>
				{countOfSelectedCountries > 0 && <SendBox 
					data={data} 
					countOfSelectedCountries={countOfSelectedCountries} />}
			</span>
			<div>{regionsList.map((item, index) => {
					return <div key={index}
								className={item.isSelected ? "region-box-selected unselectable" : "region-box unselectable"}
								onClick={() => onClickRegion(item)}
								>{"in " + item.name}
								{/* >{"in " + item.name} #{devCounter()} */}
							</div>
			})}</div>
			<hr />
			<div>{
				data && data.map((item) => {
						let check = false;
						regionsList.forEach(reg => {
								// console.log(reg.isSelected, item.region, reg.name, item.cca3);
								if (reg.isSelected === true && reg.name === item.region) {
									check = true;
									// console.log(reg.isSelected, item.region, reg.name, item.cca3);
								};
						})
						if (check === true)	{
							// console.log(item.cca3);
							return <Country childToParent={childToParent} key={item.cca3} data={item} /> }
						else { return null }
					}
				)
			}</div>
		</div>
	);
};