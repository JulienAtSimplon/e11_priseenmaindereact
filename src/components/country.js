import React, { useEffect, useState } from 'react';
import "../index.scss"
// import { devCounter } from '../devTool';

export default function Country({data, childToParent}) {
	const [style, setStyle] = useState()

	function onClick() {
		selectFlag()
		childToParent(data)
		instantStyle()
		// console.log("country :", data.cca3, data.isSelected)
	}

	function selectFlag() {
		data.isSelected = !data.isSelected;
	}

	function instantStyle() {
		data.isSelected ? setStyle("country-box-selected unselectable") : setStyle("country-box unselectable")
	}

	useEffect(() => {
		instantStyle()
	},);

	useEffect(() => {}, [style]);

	return (
		<div key={data.cca3 + "div"} style={{display: "inline-block"}}>
			<div 
				// className={instantStyle()}
				className={style}
				onClick={onClick}
				key={data.cca3}
			>
				<img key={data.cca3 + "img"} className='flag' src={data.flags.svg} alt="flag" />
				{data.name.common}
				{/* #{devCounter()} */}
			</div>
		</div>
	);
};