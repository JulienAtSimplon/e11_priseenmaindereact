import React, { useEffect } from 'react';
import "../index.scss"
import { AttachEmail } from '@mui/icons-material';

export default function SendBox({data, countOfSelectedCountries}) {

	function onClick() {
		console.log("country :", countOfSelectedCountries)
	}

	// useEffect(() => {
    //     const selection = () => {
    //         data.forEach(element => {
    //             if (element.isSelected) {
    //                 console.log(element.name.common);
    //             }
    //         });
    //     }

    //     selection()
    // },);

    useEffect(() => {},[countOfSelectedCountries]);

	return (
            <div key="send-box" className="send-box">
                <div key="send-box-t" className='send-box-title unselectable'>
                    <h3>SendBox {countOfSelectedCountries}</h3>
                </div>
                <div key="send-box-flags" className='send-box-flags'>
                    {data && data.map((item) => {
                        if (item.isSelected) {
                            return <img key={item.cca3 + "sendBoxImg"} 
                                className='send-flag' 
                                src={item.flags.svg} 
                                alt="flag" /> }
                        else { return null }
                    })}
                </div>
                <div onClick={onClick} className='send-box-button unselectable'>
                    <AttachEmail sx={{ fontSize: 15 }} />
                </div>
            </div>
	);
};